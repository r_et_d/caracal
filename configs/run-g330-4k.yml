## Standard required input workers (always include in all yaml configs)

schema_version: 1.0.4


## Generate MS for calibration
# Compulsory worker to set up data/input/output directories.
# The prefix used for the output data products (e.g., diagnostic plots, images, etc.).
general:
  prefix: G330
  title: G330
  # Location where CARACal will write and expect to find .MS files
  msdir: msdir
  # This directory and the input .MS files within it can be read-only
  rawdatadir: ms-orig
  # Location where CARACal writes output products
  output: output-g330wide-4k
#   cabs:
#     - name: shadems_direct
#       tag: 1.7.1

# Compulsory worker to specify the input .MS files.
getdata:
  dataid: [1625501775_sdp_l0]
  extension: ms

# Compulsory worker to set up target/calibrators names.
obsconf:
  refant: auto


## Processing calibrators
# Split calibrators-only .MS files, one per input .MS file.
# This worker splits the calibrators (in preparation for cross-calibration) or the targets (in
# preparation for imaging) to new .MS files. Time and frequency averaging is available, as well as
# phase rotation to a new phase centre. Crosscalibration can be applied on the fly while splitting.
transform__calibrators:
  enable: true
#   label_out: &cal_label cal
  label_out: cal
#   field: &calibs calibrators
  field: calibrators
  split_field:
    enable: true
    col: data

# Prepare the calibrators-only .MS files for processing.
prep__calibrators:
  enable: true
#   label_in: *cal_label
  label_in: cal
#   field: *calibs
  field: calibrators
  fixuvw:
    enable: true
  clearcal: true
  specweights:
    enable: true
    mode: uniform

# Flag the calibrators-only .MS files.
flag__calibrators:
  enable: true
#   label_in: *cal_label
  label_in: cal
#   field: *calibs
  field: calibrators
  flag_autocorr:
    enable: true
  flag_shadow:
    enable: true
#   flag_spw: &lband_rfi
  flag_spw:
    enable: true
    # flagging out RFI channels from TM memo
    # chans: '*:856~940MHz, *:1180~1300MHz, *:1658~1800MHz, *:1419.8~1421.3MHz'
    # BEWARE the aggressive flagging at the end of the band where you line lies -- *:1592MHz~1610MHz,*:1616MHz~1626MHz,*:1658MHz~1800MHz
    chans: '*:856MHz~960MHz,*:1080MHz~1095MHz,*:1166MHz~1300MHz,*:1375MHz~1387MHz,*:1419.8MHz~1421.3MHz,*:1526MHz~1554MHz,*:1565MHz~1585MHz,*:1592MHz~1610MHz,*:1616MHz~1626MHz,*:1658MHz~1800MHz'
    ensure_valid: false
#   flag_mask: &lband_rfimask
  flag_mask:
    enable: true
    mask: meerkat.rfimask.npy
    uvrange: '0~1000'

# Flag bad data identified manually
flag__cal_manual:
  enable: true
#   label_in: *cal_label
  label_in: cal
#   field: *calibs
  field: calibrators
  flag_spw:
    enable: true
#     chans: '*:0.965299GHz~0.965717GHz,*:1.05161GHz~1.05245GHz,*:1.1026GHz~1.10344GHz,*:1.1118GHz~1.11221GHz,*:1.10887GHz~1.11598GHz,*:1.12768GHz~1.12831GHz,*:1.13583GHz~1.13834GHz,*:1.1421GHz~1.16592GHz,*:1.1862GHz~1.3045GHz,*:1.48964GHz~1.49298GHz,*:1.52057GHz~1.59195GHz,*:1.51827GHz,*:1.62632GHz'
    chans: '*:0.965290GHz~0.965930GHz,*:1.05161GHz~1.05245GHz,*:1.1026GHz~1.10344GHz,*:1.10887GHz~1.11598GHz,*:1.12768GHz~1.12831GHz,*:1.13583GHz~1.13834GHz,*:1.1421GHz~1.16595GHz,*:1.1862GHz~1.3045GHz,*:1.48964GHz~1.49298GHz,*:1.52057GHz~1.59195GHz,*:1.51827GHz,*:1.62632GHz'
    ensure_valid: false
  flag_time:
    enable: true
    timerange: '2021/07/05/16:17:30~2021/07/05/16:19:30,2021/07/05/16:27:00~2021/07/05/16:27:15'
    ensure_valid: false
  flag_antennas:
    enable: true
    antennas: 'm054'
    ensure_valid: false
#   flag_manual:
#     enable: true
#     rules:
#     - *1775 antenna:m00x
#     - 1557* scan:49
#     - 1568* antenna:m024;m036

# Flag RFI using AOFlagger, Tricolour, or CASA FLAGDATA with tfcrop.
flag__cal_rfi:
  enable: true
#   label_in: *cal_label
  label_in: cal
#   field: *calibs
  field: calibrators
#   flag_rfi: &flag_rfi
  flag_rfi:
    enable: true
    flagger: tricolour
    tricolour:
      strategy: khruschev.yaml

# Derive the cross-calibration tables and apply them to the calibrators.
# Carry out Cross calibration of the data (delay, bandpass and gain calibration).
crosscal:
  enable: true
#   label_in: *cal_label
  label_in: cal
#   label_cal: &gc1_label 1gc
  label_cal: 1gc
  uvrange: '>150'
  set_model:
    enable: true
    # error in 1934 model for this caracal installation, using meqtree model
#     meerkat_skymodel: false
    meerkat_skymodel: true
  primary:
    reuse_existing_gains: true
    order: KGBAKGB
#     combine: ["", "", "", null, "", "", ""]
    combine: ["scan,field", "spw", "scan,field", null, "", "", "scan"]
    solint: [inf, inf, inf, null, int, int, inf]
    calmode: [ap, ap, ap, null, ap, ap, ap]
    b_fillgaps: 70
    plotgains: true
#   secondary:
#   # Don't do the I option, rather image the calibrators
#     reuse_existing_gains: true
#     order: KGAKFI
#     apply: B
#     combine: ["scan,field", "spw", null, "", "", null]
#     solint: [inf, inf, null, 60s, 60s, null]
#     calmode: [ap, ap, null, ap, ap, null]
#     plotgains: true
#     image:
#       npix: 10000
#       cell: 0.8
#       niter: 100000
#       nchans: 8
  secondary:
    reuse_existing_gains: true
    order: KGAKF
    apply: B
    combine: ["scan,field", "spw", null, "", ""]
    solint: [inf, inf, null, 60s, 60s]
    calmode: [ap, ap, null, ap, ap]
    plotgains: true
  apply_cal:
    applyto:
      - fcal
      - bpcal
      - gcal
#      - xcal

# This worker plot the visibilities for diagnostic purpose. 
# Inspect the calibrated calibrator’s visibilities to check the quality of the
# cross-calibration.
inspect:
  enable: true
#   label_in: *cal_label
  label_in: cal
#   field: *calibs
  field: calibrators
  label_plot: 'plot_1gc'
  dirname: crosscal
  correlation: all
  standard_plotter: none
  shadems:
    enable: true
    default_column: CORRECTED_DATA
    plots:
      - desc: "plots by field"
        field: "{all_fields}"
        cnum: 100  # up 100 colours
        iter_field: true
        plots:
          # phaseball plots
          - dir: "phaseballs-{msbase}"
            plots:
              - "-x real -y imag -c CORR --corr IQUV --hline 0: --vline 0:"
              - "-x real -y imag -c SCAN_NUMBER"
              - "-x real -y imag -c ANTENNA1"
          - dir: "phaseballs-bycorr-{msbase}"
            iter_corr:
            plots:
              - "-x real -y imag -c SCAN_NUMBER"
              - "-x real -y imag -c ANTENNA1"
          # normalized phaseballs
          - dir: "normballs-{msbase}"
            col: "CORRECTED_DATA/MODEL_DATA"
            corr: "XX,YY"
            iter_corr:
            plots:
              - "-x real -y imag -c SCAN_NUMBER"
              - "-x real -y imag -c ANTENNA1"
          # block and triangle plots
          - dir: "blockplots-{msbase}"
            plots:
              - "-x BASELINE_M -y FREQ -c amp"
              - "-x ANTENNA1 -y ANTENNA2 -c SCAN_NUMBER --aaxis phase --ared std"
              - "-x ANTENNA1 -y ANTENNA2 -c SCAN_NUMBER --aaxis amp --ared mean"
          # amp/phase versus uv-distance, and uv-coverage coloured by amp/phase
          - dir: "uvdist-{msbase}"
            plots:
              - "-x UV -y amp    -c SCAN_NUMBER"
              - "-x UV -y amp    -c ANTENNA1"
              - "-x UV -y phase  -c ANTENNA1 --corr XX,YY"
              - "-x U  -y V      -c amp"
              - "-x U  -y V      -c phase --cmin -5 --cmax 5"
          # spectral plots
          - dir: "spectra-{msbase}"
            plots:
              - "-x FREQ  -y amp  -c SCAN_NUMBER"
              - "-x FREQ  -y amp  -c ANTENNA1"
              - "-x FREQ  -y real -c CORR --corr IQUV --hline 0:"
      # per-antenna plots
      - iter_ant:
        desc: "plots by antenna"
        cmap: pride
        corr: XX,YY
        plots:
          - '-x FREQ -y amp:I -c SCAN_NUMBER --cnum 100 --cmin 0 --cmax 30 --field {bpcal} --dir bpcal-iamp-byant-{msbase}'
          - '-x FREQ -y SCAN_NUMBER -a amp --ared std --dir stdamp-byant-{msbase}'
          - '-x FREQ -y SCAN_NUMBER -a phase --ared std --dir stdphase-byant-{msbase}'
      # per-scan plots
      - iter_scan:
        desc: "plots by scan"
        cmap: pride
        ared: std
        corr: XX,YY
        plots:
          - '-x ANTENNA1 -y ANTENNA2 -a amp --dir stdamp-byscan-{msbase}'
          - '-x ANTENNA1 -y ANTENNA2 -a phase --dir stdphase-byscan-{msbase}'
          - '-x BASELINE_M -y FREQ -a imag --amin 0 --amax 2 --dir stdimag-byscan-{msbase}'
    ignore_errors: true


## Imaging wideband target
# Split target-only .MS files, one per input .MS file and target, applying the
# cross-calibration on the fly.
transform__target:
  enable: true
#   label_out: &tgt_label corr
  label_out: corr
#   field: &tgt_sources target
  field: target
  split_field:
    enable: true
    # Prior to imaging, calibrated visibilities are averaged by 4 channels in frequency and to 8 second integration time.
    chan_avg: 4
    otfcal:
      enable: true
#       label_cal: *gc1_label
      label_cal: 1gc
      interpolation:
        delay_cal: nearest
        bp_cal: nearest
        gain_cal: linear
#      derotate_pa: false
#      output_pcal_ms: intermediate

# Prepare the target-only .MS files for processing.
prep__target:
  enable: true
#   label_in: *tgt_label
  label_in: corr
#   field: *tgt_sources
  field: target
  fixuvw:
    enable: true
  clearcal: true
  specweights:
    enable: true
    mode: uniform

# Flag the target-only .MS files.
flag__target:
  enable: true
#   label_in: *tgt_label
  label_in: corr
#   field: *tgt_sources
  field: target
  flag_autocorr:
    enable: true
  flag_shadow:
    enable: true
#   flag_spw: *lband_rfi
  flag_spw:
    enable: true
    # flagging out RFI channels from TM memo
    # BEWARE the aggressive flagging at the end of the band where you line lies -- *:1592MHz~1610MHz,*:1616MHz~1626MHz,*:1658MHz~1800MHz
    # rather inspect and flag those regions manually
#     chans: '*:856~880MHz , *:1658~1800MHz, *:1419.8~1421.3MHz'
#     chans: '*:856MHz~960MHz,*:1080MHz~1095MHz,*:1166MHz~1300MHz,*:1375MHz~1387MHz,*:1419.8MHz~1421.3MHz,*:1526MHz~1554MHz,*:1565MHz~1585MHz,*:1592MHz~1610MHz,*:1616MHz~1626MHz,*:1658MHz~1800MHz'
    chans: '*:856MHz~960MHz,*:1080MHz~1095MHz,*:1166MHz~1300MHz,*:1375MHz~1387MHz,*:1419.8MHz~1421.3MHz,*:1526MHz~1554MHz,*:1565MHz~1585MHz,*:1592MHz~1610MHz'
    ensure_valid: false
    # Not auto-rfi flagging, rather do it manually to flag out the lines for continuum
# #   flag_mask: *lband_rfimask
#   flag_mask:
#     enable: true
#     mask: meerkat.rfimask.npy
#     uvrange: '0~1000'
#   flag_manual:
#     enable: true
#     rules:
#       - '1557* antenna:m007'
#       - '1568* antenna:m024;m036'
# #      - '1568* scan:35 uvrange:1000~9000'
  inspect:
    enable: false
    field: target

flag__tgt_manual:
  enable: true
  label_in: corr
  field: target
  flag_spw:
    enable: true
#     chans: '*:1.1421GHz~1.16592GHz,*:1.1862GHz~1.25997GHz,*:1.52057GHz~1.59185GHz,*:1.51827GHz,*:1.62632GHz'
    chans: '*:0.965299GHz~0.965717GHz,*:1.05161GHz~1.05245GHz,*:1.1026GHz~1.10344GHz,*:1.10887GHz~1.11598GHz,*:1.12768GHz~1.12831GHz,*:1.13583GHz~1.13834GHz,*:1.1421GHz~1.16592GHz,*:1.1862GHz~1.3045GHz,*:1.48964GHz~1.49298GHz,*:1.52057GHz~1.59195GHz,*:1.51827GHz,*:1.62632GHz'
    ensure_valid: false
  flag_antennas:
    enable: true
    antennas: 'm054'
    ensure_valid: false

# flag__tgt_rfi:
#   enable: true
# #   field: *tgt_sources
#   field: target
# #   label_in: *tgt_label
#   label_in: corr
# #   flag_rfi: *flag_rfi
#   flag_rfi:
#     enable: true
#     flagger: tricolour
#     tricolour:
#       strategy: khruschev.yaml

# This worker creates an a-priori clean mask based on NVSS or SUMSS, to be used during the continuum
# imaging/self-calibration loop.
mask:
  enable: false

## Iteratively image the radio continuum emission and self-calibrate the visibilities
# This worker performs continuum imaging and standard (i.e., direction-independent) self-calibration.
# Make a continuum image of each target, self-calibrate, and transfer both gains
# and continuum model to the full-frequency-resolution target-only .MS files.
selfcal:
  enable: true
  # label of the MS file to process
  label_in: corr
  # nr of pixels in ouput image
  img_npix: 10000
  # image pixel size
  img_cell: 0.8
  # nr channels in output image
  img_nchans: 8
  # join channels to create MFS image
  img_joinchans: true
  # switch off multi-scale cleaning
  img_multiscale: false
  # nr of cleaning iterations
  img_niter: 100000
  # nr of spectral polynomial terms to fit each clean component
  img_specfit_nrcoeff: 4
  # chunk data up by this nr of timeslots
  # (limits the amount of data processed at once)
  cal_timeslots_chunk: 12
  # imaging parameters
  image:
    enable: true
  # calibration parameters
  calibrate_with: cubical
  calibrate:
    enable: true
    # G-Jones time solution interval
    gsols_timeslots: [4]
    # G-Jones frequency solutions
    gsols_chan: [256]
    # Phase only calibration
    gain_matrix_type: [GainDiagPhase, GainDiagPhase]
  # parameters that only apply when using CubiCal for calibration
  cal_cubical:
    dist_max_chunks: 12
  # transfer the model from the last WSClean imaging run to the MODEL_DATA column of the next MS
  transfer_model:
    enable: false

# # Perform direction-dependent calibration on the data
# ddcal:
#   enable: false
# #   label_in: *tgt_label
#   label_in: corr
#   use_pb: true
#   calibrate_dd:
#     enable: True
#     de_sources_mode: manual
#   copy_data:
#     enable: true
#   image_wsclean:
#     enable: true
#   transfer_model_dd:
#     enable: true

# -fin-
